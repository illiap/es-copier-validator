from requests_aws4auth import AWS4Auth
import boto3
import requests

host = 'https://vpc-qa-products-zzgfbu6kxm6hajiyyft6vy6z5y.us-east-1.es.amazonaws.com/' # The domain with https:// and trailing slash. For example, https://my-test-domain.us-east-1.es.amazonaws.com/
path = 'qa_zitem/_search' # the OpenSearch API endpoint
region = 'us-east-1' # For example, us-west-1

service = 'es'
credentials = boto3.Session().get_credentials()
awsauth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service, session_token=credentials.token)

url = host + path

r = requests.get(url, auth=awsauth) # requests.get, post, and delete have similar syntax

print(r.text)