from requests_aws4auth import AWS4Auth
import boto3
import requests

host = 'https://vpc-qa-products-zzgfbu6kxm6hajiyyft6vy6z5y.us-east-1.es.amazonaws.com/' # The domain with https:// and trailing slash. For example, https://my-test-domain.us-east-1.es.amazonaws.com/
path = '' # the OpenSearch API endpoint
region = 'us-east-1' # For example, us-west-1

service = 'es'
session = boto3.Session()
sts = session.client("sts")
role = sts.assume_role(
    RoleArn="arn:aws:iam::266423484558:role/qa-readers-role",
    RoleSessionName="test"
)
print(role)
credentials = role['Credentials']
awsauth = AWS4Auth(credentials['AccessKeyId'], credentials['SecretAccessKey'], region, service, session_token=credentials['SessionToken'])

url = host + path

# The JSON body to accompany the request (if necessary)

r = requests.get(url, auth=awsauth) # requests.get, post, and delete have similar syntax

print(r.text)