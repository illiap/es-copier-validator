from requests_aws4auth import AWS4Auth
import boto3
import requests

host = 'https://vpc-qa-products-zzgfbu6kxm6hajiyyft6vy6z5y.us-east-1.es.amazonaws.com/' # The domain with https:// and trailing slash. For example, https://my-test-domain.us-east-1.es.amazonaws.com/
path = 'qa_zitem' # the OpenSearch API endpoint
region = 'us-east-1' # For example, us-west-1
print("Authenticating...")
service = 'es'
credentials = boto3.Session().get_credentials()
awsauth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service, session_token=credentials.token)
print(f"Authentication successful awsauth={awsauth}!")
url = host + path + "/_doc/_search"

# The JSON body to accompany the request (if necessary)
print(f"Sending request to {url}...")
r = requests.get(url, auth=awsauth) # requests.get, post, and delete have similar syntax
print("Request successful. Response:")
print(r.text)