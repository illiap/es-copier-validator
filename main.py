from requests import get, post
import logging
import json

logging.basicConfig(filename='zitem_3m_test.log', level=logging.ERROR)


def convert_to_bool(r, rid, key):
    if key in r:
        if r[key] is "Y" or r[key] == "True":
            r[key] = True
        elif r[key] is "N" or r[key] == "False":
            r[key] = False
        elif r[key] is "":
            pass
        else:
            logging.error(f'id - {rid}, key - {key}, value - {r[key]} cannot be converted to bool!!!')
            raise Exception("Skipping")


def convert_record(r, rid):
    r.pop('categorization', None)
    r.pop('categoryPaths', None)
    convert_to_bool(r, rid, "circleEIndicator")
    convert_to_bool(r, rid, "CPSCCertificateFlag")
    convert_to_bool(r, rid, "nonexpeditableOverride")
    convert_to_bool(r, rid, "refrigerantFlag")
    convert_to_bool(r, rid, "MSDSIndicator")

    return r


if __name__ == "__main__":
    prod = "http://search-products.zoro.com/zoro"
    local = "http://localhost:9200"
    index = "zitem01"

    with open("data/my_index_data_prod.json") as f:
        for line in f:
            record = json.loads(line)
            try:
                updated_record = convert_record(record['_source'], record['_id'])
            except Exception:
                pass

            try:
                response = post(url=f"{local}/{index}/_doc", data=json.dumps(updated_record), headers={'Content-Type': 'application/json'})
                if response.status_code != 201:
                    logging.error(f"id - {record['_id']} status code is not 201!!!, text - {response.text}")
            except Exception as e:
                logging.error(f"id - {record['_id']} cannot be inserted!!!, message - {str(e)}")
