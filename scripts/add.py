import json
import config
from datetime import datetime
from requests import post, put, delete

if __name__ == "__main__":
    for index_key, index_config in config.index_config.items():
        with open(config.index_config[index_key]['source_additional_data_file_path']) as f:
            for line in f:
                record = json.loads(line)
                if config.index_config[index_key].get('transform_name'):
                    record["_source"]["modifiedDate"] = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
                    record["_source"]["salesStatus"] = "ADDED!!!"
                post(
                    url=f"{config.source_url}/{config.index_config[index_key]['source_name']}/_doc/{record['_id']}",
                    data=json.dumps(record["_source"]),
                    headers={'Content-Type': 'application/json'}
                )

        print(f"URL - {config.source_url} index - {config.index_config[index_key]['source_name']} records added")



