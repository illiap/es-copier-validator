# DEV
# source_url = "http://search-products-qa.zoro.com:80"
# target_url = "http://search-products-qa.zoro.com:80"
#
# index_config = {
#     "zitem": {
#         "source_name": "dev_es6_vac_zitem01",
#         "target_name": "dev_es7_vac_zitem01",
#         "source_mapping": 'es6_zitem.json',
#         "target_mapping": 'es7_zitem_es6cluster.json',
#         "source_initial_data_file_path": "../data/20_test.json",
#         "source_additional_data_file_path": "../data/10_test.json",
#         "transform_name": 'product-cache'
#     },
#     "zredirect": {
#         "source_name": "dev_es6_vac_zredirect01",
#         "target_name": "dev_es7_vac_zredirect01",
#         "source_mapping": 'es6_zredirect.json',
#         "target_mapping": 'es7_zredirect_es6cluster.json',
#         "source_initial_data_file_path": "../data/7_redirects.json",
#         "source_additional_data_file_path": "../data/3_redirects.json",
#     }
# }

# QA
source_url = "http://search-products-qa.zoro.com:80"
target_url = "http://search-products-qa.zoro.com:80"

index_config = {
    "zitem": {
        "source_name": "qa_es6_vac_zitem01",
        "target_name": "qa_es7_vac_zitem01",
        "source_mapping": 'es6_zitem.json',
        "target_mapping": 'es7_zitem_es6cluster.json',
        "source_initial_data_file_path": "../data/20_test.json",
        "source_additional_data_file_path": "../data/10_test.json",
        "transform_name": 'product-cache'
    },
    "zredirect": {
        "source_name": "qa_es6_vac_zredirect01",
        "target_name": "qa_es7_vac_zredirect01",
        "source_mapping": 'es6_zredirect.json',
        "target_mapping": 'es7_zredirect_es6cluster.json',
        "source_initial_data_file_path": "../data/7_redirects.json",
        "source_additional_data_file_path": "../data/3_redirects.json",
    }
}
