import os
import json
import logging
import requests
import config
from datetime import datetime

from transformer.transfomation_processor import apply_transform

logging.basicConfig(filename=f'copy_{datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}.log', level=logging.ERROR)


def copy(date=None):
    # query = {"range": {"modifiedDate": {"gte": date}}} if date else {"match_all": {}}
    query = {"match_all": {}}

    date_copy_started = datetime.now().strftime("%m-%d-%Y %H:%M:%S")

    for index_key, index_config in config.index_config.items():
        get_response = requests.get(
            url=f"{config.source_url}/{config.index_config[index_key]['source_name']}/_search",
            params={"scroll": "1m"},
            data=json.dumps({"size": "100", "query": query}),
            headers={'Content-Type': 'application/json'}
        ).json()

        number_of_pages = 0
        while len(get_response["hits"]["hits"]) > 0:
            records = get_response["hits"]["hits"]

            for r in records:
                # time.sleep(1)
                if config.index_config[index_key].get('transform_name'):
                    apply_transform(r['_source'], config.index_config[index_key].get('transform_name'))
                updated_record = r['_source']

                try:
                    post_response = requests.post(
                        url=f"{config.target_url}/{config.index_config[index_key]['target_name']}/_doc/{r['_id']}",
                        data=json.dumps(updated_record),
                        headers={'Content-Type': 'application/json'}
                    )
                    if post_response.status_code != 201 and post_response.status_code != 200:
                        logging.error(f"id - {r['_id']} status code is not 201!!!, text - {post_response.text}")
                        print(f"id - {r['_id']} status code is not 201!!!!!!!!!!!, text - {post_response.text}")
                except Exception as e:
                    logging.error(f"id - {post_response['_id']} cannot be inserted!!!, message - {str(e)}")

            get_response = requests.get(
                url=f"{config.source_url}/_search/scroll",
                headers={'Content-Type': 'application/json'},
                data=json.dumps({"scroll": "1m", "scroll_id": get_response["_scroll_id"]})
            ).json()

            print(f"URL - {config.source_url} index - {config.index_config[index_key]['source_name']} "
                  f"was copied to URL - {config.target_url} index - {config.index_config[index_key]['target_name']}")

            number_of_pages += 1

    return number_of_pages, date_copy_started


if __name__ == "__main__":
    last_copy_date = None
    if os.path.exists("../last_copy.json"):
        with open("../last_copy.json") as f:
            last_copy_date = json.load(f)["date"]

    copy(last_copy_date)
