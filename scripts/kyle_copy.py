import json
from typing import Optional, Iterable, List
from dataclasses import dataclass, field
import argparse
import os
import os.path
import logging
import requests

import elasticsearch
import elasticsearch.helpers
from scripts.converter import convert
# from deepdiff import DeepDiff


logger = logging.getLogger(__name__)


@dataclass
class CacheConfig:
    product_url: str
    category_url: str
    product_index: str = 'zoro'
    category_index: str = 'zoro'
    product_doctype: str = 'zitem'
    category_doctype: str = 'zcategory'

    def product_instance(self):
        return Instance(
            client=elasticsearch.Elasticsearch(self.product_url),
            index=self.product_index,
            doctype=self.product_doctype,
        )

    def category_instance(self):
        return Instance(
            client=elasticsearch.Elasticsearch(self.category_url),
            index=self.category_index,
            doctype=self.category_doctype,
        )


@dataclass
class Instance:
    client: elasticsearch.Elasticsearch
    index: str
    doctype: str
    total_pages: int = None
    scroll_lifetime: str = "30s"
    items_per_page: int = 1000
    query = {"match_all": {}}



@dataclass
class QueryOptions:
    """Options that filter or limit what documents are processed. Documents that don't match shouldn't be recorded as
    processed nor as failed."""
    limit: Optional[int] = None


@dataclass
class Document:
    id: str
    source: dict


@dataclass
class DocError:
    id: str
    err_title: str
    message: str
    detail: str = ""


@dataclass
class ProcessResults:
    documents_processed: int = 0
    validation_errors: List[DocError] = field(default_factory=list)
    indexing_errors: List[DocError] = field(default_factory=list)

    @property
    def total_documents(self):
        return self.documents_processed + len(self.validation_errors) + len(self.indexing_errors)

    @property
    def documents_invalid(self):
        return len(self.validation_errors)

    @property
    def documents_not_indexed(self):
        return len(self.indexing_errors)

# TODO: Remove directory streams in favor of a DB (SQLite) stream or something like that


# Data streaming producers and consumers
def stream_in(input: Instance, filters: QueryOptions):
    """Stream in documents from an Elasticsearch index and doctype. Filters may be set to limit which documents are
    read."""
    hits = elasticsearch.helpers.scan(
        client=input.client,
        index=input.index,
        doc_type=input.doctype,
        query={},  # TODO: Apply query/filter options
    )

    count = 0
    for h in hits:
        yield Document(id=h["_id"], source=h["_source"])
        count += 1
        if filters.limit is not None and count >= filters.limit:
            break


def stream_in_scroll(input: Instance, scroll_id=None):
    if scroll_id:
        result = input.client.scroll(
            scroll_id=scroll_id,
            scroll=input.scroll_lifetime
        )
    else:
        result = input.client.search(
            index=input.index,
            doc_type=input.doctype,
            scroll=input.scroll_lifetime,
            size=input.items_per_page,
            query=input.query
        )

    return result


def stream_out(reporter: ProcessResults, output: Instance, docs: Iterable[Document]):
    """Stream out documents from an iterable to an Elasticsearch index and doctype."""
    def _gen():
        for d in docs:
            yield {
                "_op_type": "index",
                "_index": output.index,
                "_type": output.doctype,
                "_id": d.id,
                "_source": convert(d.source, d.id, logger)
            }

    for ok, action in elasticsearch.helpers.streaming_bulk(output.client, _gen(), raise_on_error=False):
        if ok:
            reporter.documents_processed += 1
        else:
            err = action["index"]["error"]
            reporter.indexing_errors.append(DocError(
                id=action["index"]["_id"],
                err_title=err["type"],
                message=err["reason"],
                detail=str(err.get("caused_by", ""))
            ))


def transform(docs: Iterable[Document]):
    for doc in docs:
        doc.source = convert(doc.source, doc.id, logger)
        yield doc


# def validate(docs: Iterable[Document]):
#     for doc in docs:
#         if DeepDiff(doc)
#
#         if doc is False:
#             yield Document()
#         else:
#             continue


def load_from_fs(directory, filters: QueryOptions):
    """Load JSON documents from a directory and return a generator. Documents will be all files ending in a `json`
    extension. The contents will be treated as the source to sent to Elasticsearch, the file name without the
    extension will be used as the document ID. Any subdirectories or files not ending in a json extension will be
    ignored."""
    count = 0
    with os.scandir(directory) as dirinfo:
        for entry in dirinfo:
            fid, dot, ext = entry.name.rpartition(".")
            if ext == "json" and entry.is_file():
                with open(entry.path, mode="r") as f:
                    src = json.load(f)
                yield Document(id=fid, source=src)
                count += 1
                if filters.limit is not None and count >= filters.limit:
                    break


def save_to_fs(reporter: ProcessResults, directory, docs: Iterable[Document]):
    """Write out documents to a directory. Each document will be written to a separate file, with the document ID used
    as the file name with a `json` extension suffixed, and the contents being the document source. Any existing files
    with names that collide will be overwritten, and any other files or subdirectories will be unaffected."""
    for d in docs:
        filepath = os.path.join(directory, f"{d.id}.json")
        with open(filepath, mode="w") as f:
            json.dump(d.source, f)
        reporter.documents_processed += 1


# Overall flow
def process(
        filters: QueryOptions,
        in_instance: Optional[Instance] = None,
        in_directory: Optional[str] = None,
        out_instance: Optional[Instance] = None,
        out_directory: Optional[str] = None,
) -> ProcessResults:
    # Check that exactly one input and one output is chosen
    assert in_instance or in_directory, "No input sources requested"
    assert not in_instance or not in_directory, "Multiple input sources requested"
    assert out_instance or out_directory, "No output sinks requested"
    assert not out_instance or not out_directory, "Multiple output sinks requested"

    # Prepare the input generator
    if in_instance:
        # in_generator = stream_in(in_instance, filters)
        scroll_results = stream_in_scroll(in_instance)
    elif in_directory:
        in_generator = load_from_fs(in_directory, filters)
    else:
        raise RuntimeError("How did I get here?")

    results = ProcessResults()
    transformed_generator = transform(in_generator)
    # validated_generator = validate(transformed_generator)

    # Pipe it to the output handler
    if out_instance:
        stream_out(results, out_instance, transformed_generator)
    elif out_directory:
        save_to_fs(results, out_directory, in_generator)
    else:
        raise RuntimeError("How did I get here?")

    return results


# CLI Script Usage
def make_es_instance(parser: argparse.ArgumentParser, combo_url: str) -> Instance:
    parts = combo_url.rsplit("/", maxsplit=2)
    if len(parts) != 3:
        parser.error("An Elasticsearch base URL must include index and doctype\n"
                     "Like: http://cluster.example.com/index/doctype")
    return Instance(
        client=elasticsearch.Elasticsearch(parts[0]),
        index=parts[1],
        doctype=parts[2],
    )


def main():
    parser = argparse.ArgumentParser(
        description="Stream in/out Elasticsearch documents",
    )
    # Source/sink parameters
    in_group = parser.add_mutually_exclusive_group(required=True)
    in_group.add_argument("--in-es-url", metavar="URL",
                        help="ES base URL to stream from (as '<host>/<index>/<doctype>')")
    in_group.add_argument("--in-directory", metavar="PATH", help="Directory to load files from")
    out_group = parser.add_mutually_exclusive_group(required=True)
    out_group.add_argument("--out-es-url", metavar="URL",
                        help="ES base URL to stream to (as '<host>/<index>/<doctype>')")
    out_group.add_argument("--out-directory", metavar="PATH", help="Directory to load files to")
    # Filter parameters
    parser.add_argument("--limit", metavar="N", type=int, help="Limit to N documents (unlimited if not specified)")

    logging.basicConfig(format="[%(levelname)s] %(name)s: %(message)s", level=logging.INFO)
    # Parse and validate CLI arguments
    args = parser.parse_args()
    if args.in_directory:
        in_directory = args.in_directory
        in_instance = None
        if not os.path.isdir(args.in_directory):
            parser.error("The input directory path must be an existing directory")
    elif args.in_es_url:
        in_instance = make_es_instance(parser, args.in_es_url)
        in_directory = None
        logger.info(f"Checking input ES cluster at {args.in_es_url}")
        if not in_instance.client.ping():
            parser.error("Cannot connect to input ES cluster")
        elif not in_instance.client.indices.exists(in_instance.index):
            parser.error(f"Index '{in_instance.index}' doesn't exist on input cluster")
    else:
        raise RuntimeError("How did I get here?")

    if args.out_directory:
        out_directory = args.out_directory
        out_instance = None
        if not os.path.isdir(args.out_directory):
            if os.path.exists(args.out_directory):
                parser.error("The output directory path must be a directory")
            else:
                os.mkdir(args.out_directory)
    elif args.out_es_url:
        out_instance = make_es_instance(parser, args.out_es_url)
        out_directory = None
        logger.info(f"Checking output ES cluster at {args.out_es_url}")
        if not out_instance.client.ping():
            parser.error("Cannot connect to output ES cluster")
        elif not out_instance.client.indices.exists(out_instance.index):
            parser.error(f"Index '{out_instance.index}' doesn't exist on output cluster")
    else:
        raise RuntimeError("How did I get here?")

    query_opts = QueryOptions()
    if args.limit:
        if args.limit < 1:
            parser.error("Limit, if provided, must be a positive integer")
        query_opts.limit = args.limit

    results = process(query_opts, in_instance, in_directory, out_instance, out_directory)
    print(f"Processed {results.total_documents} total documents, "
          f"{results.documents_processed} valid, {results.documents_invalid} invalid, "
          f"{results.documents_not_indexed} couldn't be indexed")
    if results.validation_errors:
        print("These documents failed validation:")
        for err in results.validation_errors:
            print(f"{err.id}: {err.message}")
    if results.indexing_errors:
        print("These documents validated locally, but couldn't be indexed:")
        for err in results.indexing_errors:
            print(f"{err.id} - {err.err_title}: {err.message}")


if __name__ == "__main__":
    main()
