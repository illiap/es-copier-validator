import json
import config
from datetime import datetime
import requests

if __name__ == "__main__":
    modify_records = [
        {
            "_id": "G402603813",
            "_index": "zitem",
            "field": "manufacturerName",
            "newValue": "TEST TEST HAHAHA!" # "LINEN AVENUE"
        },
        {
            "_id": "G902602071",
            "_index": "zitem",
            "field": "price",
            "newValue": 111.11 # 445.89
        },
        {
            "_id": "G102633745",
            "_index": "zitem",
            "field": "circleEIndicator",
            "newValue": "Y" # ""
        },
        {
            "_id": "G102634827",
            "_index": "zitem",
            "field": "refrigerantFlag",
            "newValue": "Y" # "N"
        },
        {
            "_id": "G502602976",
            "_index": "zitem",
            "field": "brand",
            "newValue": "BRAND HAHAHA!" # "LINEN AVENUE"
        },
        {
            "_id": "G002603107",
            "_index": "zitem",
            "field": "cost",
            "newValue": 99.99 # 107.64
        },
        {
            "_id": "G502633888",
            "_index": "zitem",
            "field": "nonexpeditableOverride",
            "newValue": "True" # null
        },
        {
            "_id": "G502633888",
            "_index": "zitem",
            "field": "comment",
            "newValue": "VERY SMART COMMENT! HAHAH test"  # null
        },
        {
            "_id": "c3VtaXRvbW8tNTJndDE4LXR1cm5pbmctaW5zZXJ0ZG5tYWNibjQzMS1zaXplL2cvMDA2MzEzMTY",
            "_index": "zredirect",
            "field": "oldPath",
            "newValue": "HAHAHAHAH!!!" # /sumitomo-52gt18-turning-insertdnmacbn431-size/g/00631316/
        },
        {
            "_id": "c3VtaXRvbW8tNTJndDE0LXR1cm5pbmctaW5zZXJ0ZGNnYWNibjMyNTMtc2l6ZS9nLzAwNjMxMzIw",
            "_index": "zredirect",
            "field": "newPath",
            "newValue": "OLOLO" # /turning-inserts/c/8646/
        }
    ]

    for r in modify_records:
        es6_get_response = requests.get(
            url=f"{config.source_url}/{config.index_config[r['_index']]['source_name']}/_doc/{r['_id']}",
            headers={'Content-Type': 'application/json'}
        ).json()

        es6_get_response['_source'][r['field']] = r['newValue']

        requests.post(
            url=f"{config.source_url}/{config.index_config[r['_index']]['source_name']}/_doc/{r['_id']}",
            data=json.dumps(es6_get_response["_source"]),
            headers={'Content-Type': 'application/json'}
        )

    print(f"URL - {config.source_url} records modified")



