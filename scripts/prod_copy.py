import os
import json
import logging
import requests
import time
from datetime import datetime

from converter import convert

logging.basicConfig(filename=f'copy_{datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}.log', level=logging.ERROR)


def timeit(func):
    """
    Decorator for measuring function's running time.
    """
    def measure_time(*args, **kw):
        start_time = time.time()
        result = func(*args, **kw)
        print("Processing time of %s(): %.2f seconds."
              % (func.__qualname__, time.time() - start_time))
        return result

    return measure_time


@timeit
def copy():
    query = {"match_all": {}}

    date_copy_started = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
    get_response = requests.get(
        url=f"{es6_prod}/zoro/zitem/_search",
        params={"scroll": "1m"},
        data=json.dumps({"size": "100", "query": query}),
        headers={'Content-Type': 'application/json'}
    ).json()

    number_of_pages = 0
    while len(get_response["hits"]["hits"]) > 0:
        records = get_response["hits"]["hits"]

        for r in records:
            # time.sleep(1)
            try:
                updated_record = convert(r['_source'], r['_id'], logging)
            except Exception:
                continue

            try:
                post_response = requests.post(
                    url=f"{es7}/{index_name}/_doc/{r['_id']}",
                    data=json.dumps(updated_record),
                    headers={'Content-Type': 'application/json'}
                )
                if post_response.status_code != 201:
                    logging.error(f"id - {r['_id']} status code is not 201!!!, text - {post_response.text}")
            except Exception as e:
                logging.error(f"id - {post_response['_id']} cannot be inserted!!!, message - {str(e)}")

        get_response = requests.get(
            url=f"{es6_prod}/_search/scroll",
            headers={'Content-Type': 'application/json'},
            data=json.dumps({"scroll": "1m", "scroll_id": get_response["_scroll_id"]})
        ).json()

        number_of_pages += 1
        logging.error(f'FINISHED COPY {datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}')
        if number_of_pages >= 70:
            break

    return number_of_pages, date_copy_started


if __name__ == "__main__":
    index_name = "zitem01"
    es6_prod = "http://search-products.zoro.com"
    es7 = "http://localhost:9207"

    copy()

    print("Finished copy!")
