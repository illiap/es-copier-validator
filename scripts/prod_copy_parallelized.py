import json
import logging
import threading
import time
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

import requests

from scripts.converter import convert

logging.basicConfig(filename=f'copy_paral_{datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}.log', level=logging.ERROR)
logger = logging.getLogger(__name__)

total_pages = 100
items_per_page = "1000"
number_of_threads = 4
scroll_lifetime = "30s"


class Counter:
    def __init__(self):
        self.lock = threading.Lock()
        self.number_of_pages = 0

    def increment(self):
        self.lock.acquire()
        self.number_of_pages += 1
        self.lock.release()


def process_get_response(get_response, counter):
    records = get_response["hits"]["hits"]

    for r in records:
        # time.sleep(1)
        try:
            updated_record = convert(r['_source'], r['_id'], logger)
        except Exception as e:
            logger.error(str(type(e)) + str(e))
            continue

        # try:
        #     post_response = requests.post(
        #         url=f"{es7}/{index_name}/_doc/{r['_id']}",
        #         data=json.dumps(updated_record),
        #         headers={'Content-Type': 'application/json'}
        #     )
        #     if post_response.status_code != 201:
        #         logger.error(f"id - {r['_id']} status code is not 201!!!, text - {post_response.text}")
        # except Exception as e:
        #     logger.error(f"id - {r['_id']} cannot be inserted!!!, message - {str(e)}")


def process_scroll(scroll_id, counter):
    logger.error(f'STARTING THREAD - {threading.get_ident()} - {datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}')
    retry = 0
    while True:
        if counter.number_of_pages >= total_pages:
            logger.error(f'Number of pages exceeds {total_pages} - {threading.get_ident()} - {datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}')
            break
        counter.increment()

        get_response = requests.get(
            url=f"{es6_prod}/_search/scroll",
            headers={'Content-Type': 'application/json'},
            data=json.dumps({"scroll": scroll_lifetime, "scroll_id": scroll_id})
        ).json()

        # if len(get_response["hits"]["hits"]) <= 0 or n.number_of_pages >= 70:
        try:
            if len(get_response["hits"]["hits"]) <= 0:
                if retry <= 3:
                    time.sleep(.25)
                    retry += 1
                    continue
                else:
                    logger.error(f'Number of hits is 0 - {threading.get_ident()} - {datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}')
                    break
        except Exception:
            logger.error(f'GET RESPONSE!!! - {get_response}')
            t = 1

        process_get_response(get_response, counter)
        logger.error(f'FINISHED COPY - {threading.get_ident()} - {datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}')
    logger.error(f'JOINING THREAD - {threading.get_ident()} - {datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}')


def copy():
    now = time.time()
    query = {"match_all": {}}
    counter = Counter()

    date_copy_started = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
    get_response = requests.get(
        url=f"{es6_prod}/zoro/zitem/_search",
        params={"scroll": scroll_lifetime},
        data=json.dumps({"size": items_per_page, "query": query}),
        headers={'Content-Type': 'application/json'}
    ).json()

    if len(get_response["hits"]["hits"]) <= 0:
        return date_copy_started
    else:
        process_get_response(get_response, counter)

    # threads = []
    #
    # for i in range(number_of_threads):
    #     t = threading.Thread(target=process_scroll, args=(get_response["_scroll_id"], counter))
    #     threads.append(t)
    #
    # for t in threads:
    #     time.sleep(1)
    #     t.start()
    #
    # for t in threads:
    #     t.join()

    with ThreadPoolExecutor(max_workers=4) as e:
        e.submit(process_scroll, get_response["_scroll_id"], counter)
        time.sleep(1)
        e.submit(process_scroll, get_response["_scroll_id"], counter)
        time.sleep(1)
        e.submit(process_scroll, get_response["_scroll_id"], counter)
        time.sleep(1)
        e.submit(process_scroll, get_response["_scroll_id"], counter)

    logger.error(f'Number of pages: {counter.number_of_pages}')
    logger.error(f'Elapsed time: {time.time() - now}')

    return date_copy_started


if __name__ == "__main__":
    index_name = "zitem01"
    es6_prod = "http://search-products.zoro.com"
    es7 = "http://localhost:9207"

    copy()

    print("Finished copy!")
