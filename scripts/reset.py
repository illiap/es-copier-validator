import json
import config
from requests import post, put, delete

if __name__ == "__main__":
    for index_key, index_config in config.index_config.items():
        delete(f"{config.source_url}/{config.index_config[index_key]['source_name']}")
        with open(f"../mappings/{config.index_config[index_key]['source_mapping']}") as es6_mapping_file:
            put(
                url=f"{config.source_url}/{config.index_config[index_key]['source_name']}",
                data=es6_mapping_file.read(),
                headers={'Content-Type': 'application/json'}
            )

        with open(config.index_config[index_key]['source_initial_data_file_path']) as f:
            for line in f:
                record = json.loads(line)
                r = post(
                    url=f"{config.source_url}/{config.index_config[index_key]['source_name']}/_doc/{record['_id']}",
                    data=json.dumps(record["_source"]),
                    headers={'Content-Type': 'application/json'}
                )

        print(f"URL - {config.source_url} index - {config.index_config[index_key]['source_name']} has been reset")

        delete(f"{config.target_url}/{config.index_config[index_key]['target_name']}")
        with open(f"../mappings/{config.index_config[index_key]['target_mapping']}") as es7_mapping_file:
            r = put(
                url=f"{config.target_url}/{config.index_config[index_key]['target_name']}",
                data=es7_mapping_file.read(),
                headers={'Content-Type': 'application/json'}
            )

        print(f"URL - {config.target_url} index - {config.index_config[index_key]['target_name']} has been reset")
