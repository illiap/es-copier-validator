# TODO Remove this transformer folder once we are able to have shared code between the CFs in the repo

from jsonpath_ng import parse
import enum


class PayloadTransformer:
    # Some built in common transforms
    built_in_transforms = {
        "to_string": lambda x: str(x),
        "to_bool": lambda x: True if x and str(x).strip().lower() in {"true", "y", "yes", "1", "t"} else False,
        "to_int": lambda x: int(x),
        "to_float": lambda x: float(x)
    }

    class Transform(enum.Enum):
        value = 1
        collection = 2

    # Initialize with the payload and a set of transforms
    # (defaults to th built ins from above)
    def __init__(self, payload, transform_func_dict=None):
        self.payload_dict = payload
        self.transform_func_dict = transform_func_dict or {**self.built_in_transforms}

    # Allows the user to register any custom transforms
    def register_custom_method(self, name, func):
        if name in self.transform_func_dict:
            raise ValueError('Transform {} already exists'.format(name))
        self.transform_func_dict[name] = func

    # Only transforms values
    def transform_value(self, expression, transform):
        transform_func = self.transform_func_dict.get(transform)
        if not transform_func:
            raise ValueError("Transform '{}' is not available".format(transform))

        jsonpath_expr = parse(expression)
        jsonpath_expr.update(self.payload_dict,
                             lambda data_field, data, field: data.update({field: transform_func(data[field])}))

    # Transforms collection
    def transform_collection(self, expression, transform):
        transform_func = self.transform_func_dict.get(transform)
        jsonpath_expr = parse(expression)
        for match in jsonpath_expr.find(self.payload_dict):
            transform_func(match.value)

    def delete_fields(self, expression_lst):
        for expression in expression_lst:
            jsonpath_expr = parse(expression)
            for match in jsonpath_expr.find(self.payload_dict):
                del match.context.value[match.path.fields[0]]

    def rename_fields(self, expression_dict):
        for expression, new_field_name in expression_dict.items():
            jsonpath_expr = parse(expression)
            for match in jsonpath_expr.find(self.payload_dict):
                value = match.context.value[match.path.fields[0]]
                del match.context.value[match.path.fields[0]]
                match.context.value[new_field_name] = value

    # Applies all the transforms at once
    def transform(self, transforms_lst):
        for item in transforms_lst:
            if item[1] == PayloadTransformer.Transform.value:
                self.transform_value(item[0], item[2])
            else:
                self.transform_collection(item[0], item[2])
