from transformer.payload_transformer import PayloadTransformer
from transformer.transformation_definitions import TRANSFORMATIONS


def apply_value_modifications(transformer, operands):
    transform_lst = []
    for operand in operands:
        transform_lst.append((operand['expression'], PayloadTransformer.Transform.value, operand['action']))
    transformer.transform(transform_lst)


def apply_field_deletions(transformer, operands):
    transformer.delete_fields(operands)


def apply_rename_fields(transformer, operands):
    transformer.rename_fields(operands)


def apply_transform(data, transform_name):
    transform_lst = TRANSFORMATIONS.get(transform_name)
    if not transform_lst:
        raise ValueError('Unsupported transform name {} supplied'.format(transform_name))

    transformer = PayloadTransformer(data)
    for transform in transform_lst:
        operation = transform['operation']
        if operation == 'modify_value':
            apply_value_modifications(transformer, transform['operands'])
        elif operation == 'delete_field':
            apply_field_deletions(transformer, transform['operands'])
        elif operation == 'rename_field':
            apply_rename_fields(transformer, transform['operands'])
        else:
            raise ValueError('Unexpected operation')
