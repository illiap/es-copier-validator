TRANSFORMATIONS = {
    "product-cache": [
      {
         "operation": "modify_value",
         "operands": [
            {
               "expression": "$[*].CPSCCertificateFlag",
               "action": "to_bool"
            },
            {
               "expression": "$[*].circleEIndicator",
               "action": "to_bool"
            },
            {
               "expression": "$[*].MSDSIndicator",
               "action": "to_bool"
            },
            {
               "expression": "$[*].refrigerantFlag",
               "action": "to_bool"
            },
            {
               "expression": "$[*].nonexpeditableOverride",
               "action": "to_bool"
            },
            {
               "expression": "$[*].cost",
               "action": "to_float"
            },
            {
               "expression": "$[*].MAP",
               "action": "to_float"
            },
            {
               "expression": "$[*].originalPrice",
               "action": "to_float"
            },
            {
               "expression": "$[*].price",
               "action": "to_float"
            },
            {
               "expression": "$[*].zoroMinOrderQty",
               "action": "to_int"
            }
         ]
      },
      {
         "operation": "delete_field",
         "operands": [
            "$[*].categorization",
            "$[*].categoryPaths"
         ]
      }
    ]
}
