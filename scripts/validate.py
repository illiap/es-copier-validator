import os
import json
import logging
import requests
import time
from datetime import datetime

from converter import convert

logging.basicConfig(filename=f'validate_{datetime.now().strftime("%Y-%m-%d__%H:%M:%S")}.log', level=logging.ERROR)
list_of_validation_fields = ["manufacturerName", "zoroNo", "price", "circleEIndicator", "CPSCCertificateFlag", "nonexpeditableOverride", "refrigerantFlag", "MSDSIndicator", "brand", "cost", "graingerBrandName", "zoroMinOrderQty"]


def validate():
    # query = {"range": {"modifiedDate": {"gte": date}}} if date else {"match_all": {}}
    query = {"match_all": {}}
    validation_results = []

    date_validation_started = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
    es6_get_response = requests.get(
        url=f"{es6}/{index_name}/_search",
        params={"scroll": "1m"},
        data=json.dumps({"size": "10", "query": query}),
        headers={'Content-Type': 'application/json'}
    ).json()

    number_of_pages = 0
    while len(es6_get_response["hits"]["hits"]) > 0:
        records = es6_get_response["hits"]["hits"]

        for r in records:
            # time.sleep(1)
            es7_get_response = requests.get(
                url=f"{es7}/{index_name}/_doc/{r['_id']}",
                headers={'Content-Type': 'application/json'}
            ).json()

            if es7_get_response['found']:
                try:
                    converted_es6_record = convert(r['_source'], r['_id'], logging)
                    es7_record = es7_get_response['_source']

                    for field in list_of_validation_fields:
                        if field in converted_es6_record:
                            if field not in es7_record or converted_es6_record[field] != es7_record[field]:
                                validation_results.append({
                                    "_id": r['_id'],
                                    "missing": False,
                                    "firstDifferentFieldFound": field,
                                    "es6ModifiedDate": r['_source']['modifiedDate'] if 'modifiedDate' in r['_source'] else None
                                })

                except Exception:
                    continue
            else:
                validation_results.append({
                    "_id": r['_id'],
                    "missing": True,
                    "firstDifferentFieldFound": None,
                    "es6ModifiedDate": r['_source']['modifiedDate'] if 'modifiedDate' in r['_source'] else None
                })

        es6_get_response = requests.get(
            url=f"{es6}/_search/scroll",
            headers={'Content-Type': 'application/json'},
            data=json.dumps({"scroll": "1m", "scroll_id": es6_get_response["_scroll_id"]})
        ).json()

        number_of_pages += 1

    return validation_results


if __name__ == "__main__":
    index_name = "zitem01"
    es6 = "http://localhost:9200"
    es7 = "http://localhost:9207"

    if os.path.exists("../validation.json"):
        os.remove("../validation.json")

    results = validate()

    f = open("validation.json", "w")
    f.write(json.dumps(results))
    f.close()

    print("done")
