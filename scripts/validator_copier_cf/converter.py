from datetime import datetime


def convert(r, rid, logger):
    def convert_to_bool(key):
        if key in r:
            if r[key] is "Y" or r[key] == "True":
                r[key] = True
            elif r[key] is "N" or r[key] == "False":
                r[key] = False
            elif r[key] is "":
                pass
            else:
                logger.error(f'id - {rid}, key - {key}, value - {r[key]} cannot be converted to bool!!!')
                raise Exception("Skipping")

    r.pop('categorization', None)
    r.pop('categoryPaths', None)
    convert_to_bool("circleEIndicator")
    convert_to_bool("CPSCCertificateFlag")
    convert_to_bool("nonexpeditableOverride")
    convert_to_bool("refrigerantFlag")
    convert_to_bool("MSDSIndicator")

    r["modifiedDate"] = datetime.now().strftime("%m-%d-%Y %H:%M:%S")

    return r
