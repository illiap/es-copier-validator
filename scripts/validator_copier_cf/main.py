import logging
import json

import elasticsearch
import elasticsearch6
import elasticsearch.helpers as eshelpers

logger = logging.getLogger(__name__)


def get_ids():
    with open("./examples/local_ids.json") as f:
        ids = json.load(f)

    return ids


def main():
    index = 'zitem01'

    es6 = elasticsearch6.Elasticsearch(hosts=["http://localhost:9206"])
    es7 = elasticsearch.Elasticsearch(hosts=["http://localhost:9207"])

    # res = es6.get(index=index, id='G402603813', doc_type='_doc')

    ids = get_ids()

    hits = eshelpers.scan(
        client=es6,
        index=index,
        size=11,
        doc_type='_doc',
        body={
            "query": {
                "ids": {
                    "values": [doc_id['_id'] for doc_id in ids]
                }
            }
        }
    )

    # hits = es6.search(
    #     index=index,
    #     size=11,
    #     doc_type='_doc',
    #     body={
    #         "query": {
    #             "ids": {
    #                 "values": [doc_id['_id'] for doc_id in ids]
    #             }
    #         }
    #     }
    # )

    a = []

    for hit in hits:
        a.append(hit)

    print(1)


if __name__ == "__main__":
    main()
