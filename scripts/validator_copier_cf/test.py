import logging

import elasticsearch
import elasticsearch6
import elasticsearch.helpers as eshelpers

logger = logging.getLogger(__name__)


def main():
    index = 'zitem01'

    es6 = elasticsearch6.Elasticsearch(hosts=["http://localhost:9206"])
    es7 = elasticsearch.Elasticsearch(hosts=["http://localhost:9207"])

    res = es6.get(index=index, id='G402603813', doc_type='_doc')

    print(1)


if __name__ == "__main__":
    main()
